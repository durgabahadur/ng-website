#!/bin/bash

USER=nuglargaerten
IP=nuglargaerten.ch

set -e


#./ng_website/manage.py collectstatic

# cleanup temporary files
find . -iname "*.pyc" -exec rm {} \;

# django project
rsync  -Pavz -e "ssh -p144" --delete ng_website/ \
	$USER@$IP:/home/nuglargaerten/nuglargaerten_django/nuglargaerten_django/

# # static files
# rsync  -Pavz -e "ssh -p144" --delete collected_static/ \
# 	$USER@$IP:/home/nuglargaerten/public_html/static/
