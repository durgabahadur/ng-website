function showoverlay (eventId) {
  var element = document.getElementById(eventId);
	var body = document.body;
	body.style.overflow = 'hidden';
	element.style.visibility = 'visible';
	element.style.opacity = '1';
	element.style.background = 'rgba(1,1,1,0.5)';
}

function closeoverlay (eventId) {
  var element = document.getElementById(eventId);
  var body = document.body;
  body.style.overflow = 'auto';
  element.style.visibility = 'collapse';
  element.style.opacity = '0';
  element.style.background = 'rgba(1,1,1,0)';
}

function openEvent (element) {
  return function () {
    var body = document.body;
    body.style.overflow = 'hidden';
    element.style.visibility = 'visible';
    element.style.opacity = '1';
    element.style.background = 'rgba(1,1,1,0.5)';
  }
}

function closeEvent (element) {
  return function () {
    var body = document.body;
    body.style.overflow = 'auto';
    element.style.visibility = 'collapse';
    element.style.opacity = '0';
    element.style.background = 'rgba(1,1,1,0)';
  }
}

window.addEventListener('load', function() {
  var container = document.getElementById("div-zoom-overlay")
  if (container) { // some pages do not have any pictures/containers
    var entrySection = document.getElementById("entry-container");
    // only do overlay on images that do not function as Link
    var imgList = entrySection.querySelectorAll(".article-body > figure img");
    container.addEventListener('click', (event) => {
      (event || window.event).preventDefault();
      event.currentTarget.style.display = 'none';
    })

    for (var i = 0; i < imgList.length; i++) {
      imgList[i].addEventListener('click', (event) => {
        (event || window.event).preventDefault();
        var imageSrc = event.currentTarget.getAttribute('src')
        var container = document.getElementById("div-zoom-overlay")
        var img = document.getElementById("img-zoom-overlay")
        img.setAttribute('src', imageSrc);
        container.style.display = 'flex';
      })
    }
  }

  // zoom event detail
  // event Label List
  var eLL = document.getElementsByClassName("event summary label");
  for (var i = 0; i < eLL.length; i++) {
    var eventId = eLL[i].getAttribute('for');
    // event Detail Container
    var eDC = document.getElementById(eventId);
    eLL[i].addEventListener('click', openEvent(eDC), false)
    eDC.getElementsByClassName("close-detail")[0]
      .addEventListener('click', closeEvent(eDC), false)
  }
})
