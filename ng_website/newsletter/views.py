# coding: utf8
from django.shortcuts import render
from django.http import HttpResponse

def get_nlregistration(request):
    # if this is a POST request we need to process the form data
    if request.method == 'GET':
        return render(
                request,
                'newsletter-registration.html',
                {
                    'category': 'newsletter'
                }
            )

    else:
        return HttpResponse('Not Found', status=404)
    # if a GET (or any other method) we'll create a blank form

