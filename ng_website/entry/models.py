# coding: utf8
from django.db import models
from django.urls import reverse
from datetime import date

class BaseEntry(models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField(
            default='',
            help_text=(
                "Der Text kann mit Markdown formatiert werden. "
                "Eine Anleitung zu Markdown gibt's "
                "<a href='https://github.com/adam-p/markdown-here/"
                "wiki/Markdown-Cheatsheet'>hier</a>."
                )
            )
    order = models.PositiveIntegerField(
            default=100,
            help_text=(
                "Die Einträge werden nach dieser Nummer sortiert. Zu oberst "
                "erscheint der Eintrag mit der kleinsten Nummer."
                )
            )

    class Meta:
        abstract = True
        ordering = ['order']

    def __unicode__(self):
        return self.title


class Entry(BaseEntry):
    CATEGORIES = (
        ('home', 'home'),
        ('solila', 'solila'),
        ('mitwirken', 'mitwirken'),
        ('lernort', 'lernort'),
        ('ernte-abo', 'ernte-abo'),
    )

    category = models.CharField(
        max_length=16, choices = CATEGORIES,
    )


class Blog(BaseEntry):
    publishDate = models.DateField(default=date.today)

    class Meta:
        ordering = ['-publishDate']


class Kalender(BaseEntry):
    coordinates = models.CharField(
            max_length=200,
            blank=True,
            help_text=(
                "Wann und wo findet der Event statt?" 
                )
            )
    startDate = models.DateField(
            default=date.today,
            help_text=(
                "Wann beginnt der Event? Kalendereintrage werden nach diesem "
                "Datum sortiert."
                )
            )
    endDate = models.DateField(
            null=True, blank=True,
            help_text=(
                "Wann soll der Event aus dem Kalender verschwinden? Der "
                "Kalendereintrag wird ab diesem Datum nicht mehr angezeigt."
                "Kann auch leer gelassen werden. In dem Fall verschwindet "
                "der Eintrag nach Ablauf der Startdatums."
                )
            )
    class Meta:
        ordering = ['startDate']
