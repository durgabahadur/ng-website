from django import template
import markdown, bleach
from bleach_whitelist import markdown_tags, markdown_attrs
from markdown.extensions.toc import TocExtension

register = template.Library()

@register.simple_tag
def get_category_list(objects, category):
    """ create list of entries in the given category """
    obj = objects.filter(category=category)
    return obj


@register.simple_tag
def md_to_html(md):
    """ Convert markdown to html """
    ext=[TocExtension(baselevel=3),'yafg','attr_list','tables']
    markdown_attrs.update(video='controls', source='src, type', div='class')
    markdown_tags.extend(['a', 'div', 'figure', 'figcaption', 'img', 'video', 'source','table','thead','tr','th','td','tbody'])
    html = bleach.clean(markdown.markdown(md, extensions=ext),
                        markdown_tags, markdown_attrs)
    return html

