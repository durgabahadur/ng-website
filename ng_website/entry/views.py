from django.views.generic import View, ListView, TemplateView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.db.models import Q
from datetime import date

from entry.models import Entry, Blog, Kalender

genericFields = ['title', 'order', 'content']
kalenderFields = ['title', 'coordinates', 'startDate', 'endDate', 'content']
blogFields = ['title', 'publishDate', 'content']

def reverse_url_func(category):
    return reverse_lazy('%s:list' % category)

class BaseList(TemplateView):
    success_url = reverse_lazy('entry:list')
    breadcumbs = ['list']
    template_name = 'entry/entry_list.html'


class GenericList(BaseList):
    model = Entry
    category = ''
    def get_context_data(self, **kwargs):
        context = super(GenericList, self).get_context_data(**kwargs)
        # get url namespace and attach it to the context
        context['entry_list'] = Entry.objects.all().filter(
                category=self.category
                )
        return context


class KalenderList(BaseList):
    model = Kalender
    def get_context_data(self, **kwargs):
        context = super(KalenderList, self).get_context_data(**kwargs)
        # get url namespace and attach it to the context
        context['entry_list'] = (
                Kalender
                .objects
                .all()
                .filter(Q(startDate__gte=date.today()) | Q(endDate__gte=date.today()))
                )
        return context


class BlogList(BaseList):
    model = Blog
    def get_context_data(self, **kwargs):
        context = super(BlogList, self).get_context_data(**kwargs)
        # get url namespace and attach it to the context
        if kwargs.get('archive'):
            context['entry_list'] = Blog.objects.all()[5:]
        else:
            context['entry_list'] = Blog.objects.all()[:5]
        return context


class ListSwitcher(TemplateView):
    def dispatch(self, request, *args, **kwargs):
        if kwargs.get('category') == 'kalender':
            return KalenderList.as_view()(request, *args, **kwargs)
        elif kwargs.get('category') == 'blog':
            return BlogList.as_view()(request, *args, **kwargs)
        else:
            return GenericList.as_view(
                    category=kwargs.get('category')
                    )(request, *args, **kwargs)


class BaseCreate(PermissionRequiredMixin, CreateView):
    template_name = 'entry/entry_form.html'


class GenericCreate(BaseCreate):
    model = Entry
    fields = genericFields
    category = ''
    permission_required = 'entry.add_entry'
    def form_valid(self, form):
        # automatically assign category to model instance
        form.instance.category = self.category
        return super(GenericCreate, self).form_valid(form)


class KalenderCreate(BaseCreate):
    model = Kalender
    fields = kalenderFields
    success_url = reverse_lazy('kalender:list')
    permission_required = 'entry.add_kalender'
    pass


class BlogCreate(BaseCreate):
    model = Blog
    fields = blogFields
    success_url = reverse_lazy('blog:list')
    permission_required = 'entry.add_blog'
    pass


class CreateSwitcher(CreateView):
    def dispatch(self, request, *args, **kwargs):
        cat = kwargs.get('category')
        if cat == 'kalender':
            return KalenderCreate.as_view(
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)
        elif cat == 'blog':
            return BlogCreate.as_view(
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)
        else:
            cat = kwargs.get('category')
            return GenericCreate.as_view(
                    category = cat,
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)


class BaseUpdate(PermissionRequiredMixin, UpdateView):
    template_name = 'entry/entry_form.html'


class GenericUpdate(BaseUpdate):
    model = Entry
    fields = genericFields
    category = ''
    permission_required = 'entry.change_entry'
    pass


class KalenderUpdate(BaseUpdate):
    model = Kalender
    fields = kalenderFields
    permission_required = 'entry.change_kalender'
    pass


class BlogUpdate(BaseUpdate):
    model = Blog
    fields = blogFields
    permission_required = 'entry.change_blog'
    pass


class UpdateSwitcher(UpdateView):
    def dispatch(self, request, *args, **kwargs):
        cat = kwargs.get('category')
        if cat == 'kalender':
            return KalenderUpdate.as_view(
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)
        elif cat == 'blog':
            return BlogUpdate.as_view(
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)
        else:
            return GenericUpdate.as_view(
                    category=cat,
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)


class BaseDelete(PermissionRequiredMixin, DeleteView):
    model = Entry
    template_name = 'entry/entry_confirm_delete.html'
    permission_required = 'entry.delete_entry'


class KalenderDelete(BaseDelete):
    model = Kalender
    permission_required = 'entry.delete_kalender'
    pass


class BlogDelete(BaseDelete):
    model = Blog
    permission_required = 'entry.delete_blog'
    pass


class DeleteSwitcher(DeleteView):
    def dispatch(self, request, *args, **kwargs):
        cat = kwargs.get('category')
        if cat == 'kalender':
            return KalenderDelete.as_view(
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)
        elif cat == 'blog':
            return BlogDelete.as_view(
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)
        else:
            return BaseDelete.as_view(
                    success_url = reverse_url_func(cat)
                    )(request, *args, **kwargs)
