# $ cat 2000m2ToLernort.py | python manage.py shell
# we renamed the category 2000m2 to "lernort", so we have to adjust all entries
# pointing to 2000m2
from entry.models import Entry

twoKm2entries = Entry.objects.all().filter(category="2000m2")

for e in twoKm2entries:
    e.category = "lernort"
    e.save()
