from django.conf.urls import include, url
from entry import views

# for namespace reverse url feature, see
# https://docs.djangoproject.com/en/1.10/topics/http/urls/#id4
app_name = 'entry'

urlpatterns = [
        url(
            r'^$',
            views.ListSwitcher.as_view(),
            name='list'
            ),
        url(
            r'^new$',
            views.CreateSwitcher.as_view(),
            name='new'
            ),
        url(
            r'^edit/(?P<pk>\d+)$',
            views.UpdateSwitcher.as_view(),
            name='edit'
            ),
        url(
            r'^delete/(?P<pk>\d+)$',
            views.DeleteSwitcher.as_view(),
            name='delete'
            ),
        url(
            r'^archive$',
            views.ListSwitcher.as_view(),
            kwargs={'archive': 'true'},
            name='archive'
            ),
        ]
