"""ng_website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
"""
from django.conf.urls import include, url
from django.views.generic.base import RedirectView
from django.contrib.auth import views as auth_views
from django.contrib import admin, auth
from django.conf import settings
import ackerpatenschaft.views as ackerpatenschaftViews
import newsletter.views as nlview

urlpatterns = [
        url(
            r'^solila/',
            include('entry.urls', namespace='solila',),
            {'category': 'solila'}
            ),

        url(
            r'^angebote/',
            include('entry.urls', namespace='angebote',),
            {'category': 'angebote'}
            ),

        url(
            r'^ernte-abo/',
            include('entry.urls', namespace='ernte-abo',),
            {'category': 'ernte-abo'}
            ),

        url(
            r'^blog/',
            include('entry.urls', namespace='blog',),
            {'category': 'blog'}
            ),

        url(
            r'^rezept/',
            include('entry.urls', namespace='rezept',),
            {'category': 'rezept'}
            ),

        url(
            r'^lernort/',
            include('entry.urls', namespace='lernort',),
            {'category': 'lernort'}
            ),


        url(
            r'^policy/',
            include('entry.urls', namespace='policy',),
            {'category': 'policy'}
            ),

        url(
            r'^mitwirken/',
            include('entry.urls', namespace='mitwirken',),
            {'category': 'mitwirken'}
            ),

        url(
            r'^danggerscheen/',
            include('entry.urls', namespace='danggerscheen',),
            {'category': 'danggerscheen'}
            ),

        url(
            r'^veranstaltungen/',
            include('veranstaltungen.urls', namespace='veranstaltungen'),
            ),

        url(
            r'^ackerpatenschaft',
            ackerpatenschaftViews.get_ackerpatenschaft,
            ),
        url(
            r'^newsletter',
            nlview.get_nlregistration,
            ),

        url(r'^admin/', admin.site.urls),
        url(r'^accounts/', include('allauth.urls')),
        url(r'^auth/', include('django.contrib.auth.urls')),
        url(r'^login/$', auth_views.LoginView.as_view(template_name='login.html')),
        url(r'^kalender/', RedirectView.as_view(url='/veranstaltungen', permanent=True), name='redirect-to-veranstaltungen'),
        url(
            r'^',
            include('entry.urls', namespace='home'),
            {'category': 'home'}
            ),
        ]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
            url(r'^__debug__/', include(debug_toolbar.urls)),
            ] + urlpatterns
