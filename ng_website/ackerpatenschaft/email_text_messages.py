# coding: utf8
email_message = (u"""
Hallo %s!

Wir haben deine Anmeldung für die Ackerpatenschaft erhalten. Du möchtest eine Patenschaft für %s m2 für insgesamt SFr %s.- bestellen.

Wir bitten dich deine Zahlung mit dem Betreff: \"AckerPatenschaft %s %s / %s\"  auf folgendes Konto zu überweisen:

IBAN: CH19 0839 2000 1564 3630 2
Fördergem. f. e. sinnst. Wandel in der Landwirtschaft
Freie Gemeinschaftsbank
4001 Basel

Sobald der Betrag bei uns eingetroffen ist, erhältst du die gewünschte Urkunde/ Karte / Newsletter oder nichts :-)

Vielen herzlichen Dank!

Der Acker der Nuglar Gärten freut sich dank dir noch lange fruchtbar bleiben zu können!


""")

email_overview = (u"""
Hier nochmals Deine Bestellung im Überblick:

Die Patenschaft gilt für folgende Person:
%s %s

Für wieviele m2 Acker möchtest du eine Patenschaft bestellen?
%s

Wie oft möchstes Du die Patenschaft erneuern?
%s

Ich bestelle:
%s
""")
