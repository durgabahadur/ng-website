from django.apps import AppConfig


class AckerpatenschaftConfig(AppConfig):
    name = 'ackerpatenschaft'
