# coding: utf8
from django.shortcuts import render
from django.core.mail import send_mail

from .forms import AckerpatenschaftForm
from .email_text_messages import email_message, email_overview
import settings

def create_message(form):

    message = email_message
    cost = form.cleaned_data['area'] * 20
    message = message % (
            form.cleaned_data['sender_fname'],
            str(form.cleaned_data['area']),
            str(cost),
            form.cleaned_data['sender_fname'],
            form.cleaned_data['sender_lname'],
            form.cleaned_data['sender_email']
            )
    return message

def create_overview(form):
    overview = email_overview

    order = u""
    if form.cleaned_data['urkunde']:
        order += u"eine Urkunde für die Patenschaft (pdf)"
        order += u", "
    if form.cleaned_data['newsletter']:
        order += u"Weltacker/Nuglar Gärten Newsletter (per Mail)"
        order += u", "
    if form.cleaned_data['karte']:
        order += u"eine Geschenkkarte für die Patenschaft (per Post)"
        order += u"\n\nDie Geschenkkarte wird verschickt an:\n"
        order += form.cleaned_data['sender_fname']
        order += u" "
        order += form.cleaned_data['sender_lname']
        order += u"\n"
        order += form.cleaned_data['address_fst_line']
        order += u"\n"
        if form.cleaned_data['address_snd_line']:
            order += form.cleaned_data['address_snd_line']
            order += u"\n"
        order += form.cleaned_data['postcode']
        order += u" "
        order += form.cleaned_data['city']
        order += u"\n"
        order += form.cleaned_data['country']

    # nothing ordered
    if not (form.cleaned_data['urkunde']
            or form.cleaned_data['karte']
            or form.cleaned_data['karte']
            ):
        order = u"nichts"

    if (form.cleaned_data['benefited_is_me'] is "true"):
        benefited_fname = form.cleaned_data['benefited_fname']
        benefited_lname = form.cleaned_data['benefited_lname']
    else:
        benefited_fname = form.cleaned_data['sender_fname']
        benefited_lname = form.cleaned_data['sender_lname']

    overview = overview %(
            benefited_fname,
            benefited_lname,
            form.cleaned_data['area'],
            form.cleaned_data['reoccurrence'],
            order
            )

    return overview

def get_ackerpatenschaft(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = AckerpatenschaftForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            message = create_message(form)
            message += create_overview(form)

            # mail for customer
            send_mail(
                    u"Dein Acker wartet auf dich!",
                    message,
                    "kontakt@nuglargaerten.ch",
                    [form.cleaned_data['sender_email']],
                    auth_user='kontakt@nuglargaerten.ch',
                    auth_password=settings.common.EMAIL_PASSWORD_KONTAKT
                    )

            # mail for us
            send_mail(
                    u"%s %s hat %sm2 Acker bestellt!" % (
                        form.cleaned_data['sender_fname'],
                        form.cleaned_data['sender_lname'],
                        form.cleaned_data['area']
                        ),
                    message,
                    "kontakt@nuglargaerten.ch",
                    ["kontakt@nuglargaerten.ch"],
                    auth_user='kontakt@nuglargaerten.ch',
                    auth_password=settings.common.EMAIL_PASSWORD_KONTAKT
                    )

            return render(
                    request,
                    'merci.html',
                    {
                        'email': form.cleaned_data['sender_email'],
                        'name': form.cleaned_data['sender_fname'],
                        'category': 'lernort'
                        }
                    )

    # if a GET (or any other method) we'll create a blank form
    else:
        form = AckerpatenschaftForm()

    return render(
            request,
            'ackerpatenschaft.html',
            {'form': form, 'category': 'lernort'}
            )

