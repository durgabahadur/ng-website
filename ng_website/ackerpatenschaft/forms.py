# coding: utf8
from django import forms
# from django.forms import widgets

class AckerpatenschaftForm(forms.Form):
    sender_fname = forms.CharField(help_text='Vorame')
    sender_lname = forms.CharField(help_text='Nachname')
    sender_email = forms.EmailField(help_text='example@example.com')
    address_fst_line = forms.CharField(
            help_text='Strasse und Hausnummer',
            required=False
            )
    address_snd_line = forms.CharField(
            help_text='Zweite Adresszeile',
            required=False
            )
    postcode = forms.CharField(
            help_text='Postleitzahl',
            required=False
            )
    city = forms.CharField(
            help_text='Ortschaft',
            required=False
            )
    country = forms.CharField(
            help_text='Land',
            required=False
            )
    # Die Patenschaft gilt für folgende Person *
    benefited_is_me = forms.ChoiceField(
            widget=forms.RadioSelect,
            initial='true',
            choices=(
                ('true','für mich'),
                ('false','ein Geschenk für:'),
                )
            ) 
    benefited_fname = forms.CharField(help_text='Vorname', required=False)
    benefited_lname = forms.CharField(help_text='Nachname', required=False)

    area = forms.DecimalField(
            min_value=1, max_value=2000, max_digits=4, decimal_places=0,
            widget=forms.NumberInput(attrs={"onChange":'setTotal(value)'}),
            initial=1,
            help_text=("1m2 kostet SFr 20.-, maximal können 2000m2 bestellt "
                "werden")
            )

    reoccurrence = forms.ChoiceField(
            choices=(
                ('einmalig','einmalig'),
                ('halbjährlich','halbjährlich'),
                ('ganzhährlich','ganzhährlich')
                )
            )

    urkunde = forms.BooleanField(
            label=('eine Urkunde für die Patenschaft, gilt auch als '
                'Spendenbescheinigung (pdf)'),
            required=False
            )

    karte = forms.BooleanField(
            label='eine Geschenkkarte für die Patenschaft (per Post)',
            required=False
            )

    newsletter = forms.BooleanField(
            label='Weltacker / Nuglar Gärten Newsletter (per Mail)',
            required=False
            )


# class NameWidget(widgets.MultiWidget):
# 
#     def __init__(self, attrs=None):
#         # create choices for days, months, years
#         # example below, the rest snipped for brevity.
#         _widgets = (
#             widgets.TextInput(attrs=attrs),
#             widgets.TextInput(attrs=attrs),
#         )
#         super(NameWidget, self).__init__(_widgets, attrs)
# 
#     def decompress(self, value):
#         if value:
#             return [value.first_name, value.last_name]
#         return [None, None, None]
# 
# 
# class NameField(forms.MultiValueField):
#     def __init__(self, *args, **kwargs):
#         error_messages = {
#                 'incomplete': 'Bitte gib einen Namen ein'
#                 }
#         fields = (
#                 forms.CharField(
#                     error_messages = {
#                         'incomplete': 'Bitte gib einen Vornamen ein'
#                         },
#                     help_text = 'Bitte den Vornamen eingeben.'
#                     ),
#                 forms.CharField(
#                     error_messages = {
#                         'incomplete': 'Bitte gib einen Nachnamen ein'
#                         },
#                     help_text = 'Bitte den Nachnamen eingeben.'
#                     )
#                 )
#         super(NameField, self).__init__(
#                 error_messages=error_messages, fields=fields,
#                 require_all_fields=True, *args, **kwargs,
#                 widget=NameWidget
#                 )
# 
