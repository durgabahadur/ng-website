# -*- coding: utf-8 -*-
import datetime, os, parse, logging, traceback
from django import template
from django.conf import settings

register = template.Library()
logger = logging.getLogger(__name__)

def get_season():
    """ Determine current season of the year. Get a list of images from
    flickr that are tagged with the appropriate season and tagged to be used as
    background image. From this list choose one photo randomly and return its
    id."""

    season = "fruehling"
    today = int(datetime.datetime.today().timetuple().tm_yday)
    if today in range(1, 80):
        season = "winter"
    elif today in range(172, 264):
        season = "sommer"
    elif today in range(264, 355):
        season = "herbst"
    elif today in range(355, 366):
        season = "winter"

    return season

@register.simple_tag
def get_bg_img_url(size="medium"):
    """ Get list of sizes of a photo from flickr. Return the url to the
    image of appropriate size. """


    template = 'bg_{season}_{size}.jpg' # template of image file name

    # filter list of images of wanted size
    try:
        picL = [fname for fname in
                os.listdir(os.path.join(settings.STATIC_ROOT, 'img/bg'))
                if parse.parse(template, fname)
                and (parse.parse(template, fname))['season'] == get_season()
                and parse.parse(template, fname)['size'] == size
                ]
    except Exception as e:
        print(traceback.format_exc())
        print(e.message)
        picL = []

    if len(picL) >= 1:
        return os.path.join(settings.STATIC_URL, 'img/bg/', picL[0])
    else:
        return os.path.join(settings.STATIC_URL,
                'img/Kirschenblütenbäume1920x1200.jpg')
