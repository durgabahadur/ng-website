from django.conf import settings
import os, wget, json, flickrapi, random


flickr = flickrapi.FlickrAPI(settings.FLICKR_API_KEY, "",
            format='parsed-json')

def get_imageurl_trunk(image):
    return "https://farm%(farm)s.staticflickr.com/%(server)s/%(id)s_%(secret)s_%(size)s.jpg" % image

def get_list_of_season(season): 
    return flickr.photos.search(
                user_id=settings.FLICKR_USER_ID,
                tag_mode="AND",
                tags=('%s' % season)
                )['photos']['photo']

def get_list_of_sizes(photo_id): 
    return flickr.photos.getSizes(
                photo_id=photo_id
                )['sizes']['size']

def save_to_season_file(season, photo_id):
    # not interessted in tiny images
    sizes = [s for s in get_list_of_sizes(photo_id) 
            if int(s['height']) > 500] 
    targetPath = os.path.join(settings.STATIC_ROOT, 'img/bg')

    for s in sizes:
        photoFile = wget.download(s['source'], out=targetPath)
        os.rename(photoFile, 'bg_{}_{}x{}.jpg'.format(
            season, s['width'], s['height']
            ))

def save_all_sizes_of_image_by_id_and_season(season, photo_id):
    save_to_season_file(season, )

def get_images_with_tags(tagList):
    """ never tested the 'tags=(str(tagList))' part """
    #     flickr.photos.search(
    #         user_id=settings.FLICKR_USER_ID,
    #         tag_mode="AND",
    #         tags=(str(tagList))
    #         )['photos']['photo']
