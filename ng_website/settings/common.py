# coding: utf8

import os
from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

from .secrets import kontakt_smtp_pwd, veranstaltungen_smtp_pwd, private_key, client_email

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
SITE_ROOT = os.path.dirname(os.path.realpath(__file__))
FLICKR_API_KEY = 'd4630fab6e898fac4a954106f4216a1f'
FLICKR_USER_ID = '151747162@N04'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

LANGUAGES = [
    ('de-ch', _('German')),
]
# Application definition
LOGIN_REDIRECT_URL = '/'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'propagate': True,
        },
    }
}

INSTALLED_APPS = [
    'django.contrib.admin',
    # four dependencies of admin
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.google',

    'common_lib',
    'entry.apps.EntryConfig',
    'ackerpatenschaft.apps.AckerpatenschaftConfig',
    'newsletter.apps.NewsletterConfig',
    'veranstaltungen.apps.VeranstaltungenConfig'
]

EMAIL_USE_TLS = True
EMAIL_HOST = 'mail.infomaniak.com'
EMAIL_PORT = 587
EMAIL_PASSWORD_VERANSTALTUNGEN = veranstaltungen_smtp_pwd
EMAIL_PASSWORD_KONTAKT = kontakt_smtp_pwd

# google jwt credentials
SERVICE_ACCOUNT_PRIVATE_KEY = private_key
SERVICE_ACCOUNT_CLIENT_EMAIL = client_email

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ng_website.urls'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        },
    'veranstaltungen': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '{}/../django_cache/veranstaltungen'.format(BASE_DIR),
        'OPTIONS': {
            'MAX_ENTRIES': 10,
            }
        },
    'teilnehmende': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '{}/../django_cache/teilnehmende'.format(BASE_DIR),
        'OPTIONS': {
            'MAX_ENTRIES': 300,
            }
        }
    }

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
            'templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

		'django.template.context_processors.request',
        'entry.processor.get_entries',
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = (
        'django.contrib.auth.backends.ModelBackend',
        'allauth.account.auth_backends.AuthenticationBackend',
        )

# muss 2 bleiben, sonst gibts error 500 auf dem Server
SITE_ID = 2


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/
LANGUAGE_CODE = 'de-ch'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

# path to static folder on server (https://example.ex/STATIC_URL/)
STATIC_URL = '/static/'
