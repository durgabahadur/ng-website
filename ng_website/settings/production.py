from .common import *  # noqa: F403,401
from .secrets import db_user, db_psw, django_secret

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False
# DEBUG_PROPAGATE_EXCEPTIONS = True

ALLOWED_HOSTS = ['.nuglargaerten.ch']
# ALLOWED_HOSTS = [ '*' ]


CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
# MIDDLEWARE.append( # noqa: F405
#     'whitenoise.middleware.WhiteNoiseMiddleware'
# )
DATABASES = {
    'default': {
        "ENGINE": "django.db.backends.mysql",
        "NAME": "nuglargaerten",
        "USER": db_user,
        "PASSWORD": db_psw,
    }
}

INTERNAL_IPS = ('127.0.0.1')

SECRET_KEY = django_secret

SECURE_BROWSER_XSS_FILTER = True

SECURE_CONTENT_TYPE_NOSNIFF = True

SECURE_SSL_REDIRECT = True

SESSION_COOKIE_SECURE = True

X_FRAME_OPTIONS = 'DENY'

# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
STATIC_ROOT = os.path.join(BASE_DIR, '../../public_html/static/')
