from .common import *  # noqa: F403,401


# SECURITY WARNING: don't run with debug turned on in production!
# DEBUG = False
DEBUG = True

ALLOWED_HOSTS = [ '*' ]

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
# INSTALLED_APPS.append('debug_toolbar')

# MIDDLEWARE.append( # noqa: F405
#     'debug_toolbar.middleware.DebugToolbarMiddleware'
# )
# MIDDLEWARE.append( # noqa: F405
#     'whitenoise.middleware.WhiteNoiseMiddleware'
# )

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, '../db.sqlite3'),
    }
}

INTERNAL_IPS = ('127.0.0.1')

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

# you can put any key in here:
SECRET_KEY = "asdfj3w4r09*&sh3bd"

STATIC_ROOT = os.path.join(BASE_DIR, "../static/")
# list of folders where Django will search for additional static files aside
# from the static folder of each app installed
STATICFILES_DIRS = (os.path.join(BASE_DIR, '../collected_static'),)
