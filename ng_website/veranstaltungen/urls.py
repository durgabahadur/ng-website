from django.conf.urls import include, url
from veranstaltungen import views

# for namespace reverse url feature, see
# https://docs.djangoproject.com/en/1.10/topics/http/urls/#id4
app_name = 'veranstaltungen'

urlpatterns = [
        # url(
        #     r'^vorbei$',
        #     views.get_vergangene_veranstaltungen,
        #     name='vergangene'
        #     ),
        url(
            r'^anmeldung/(?P<eventid>\w+)$',
            views.get_anmeldung,
            name='anmeldung'
            ),
        url(
            r'^teilnehmende$',
            views.getTeilnehmende,
            name='teilnehmende'
            ),
        url(
            r'^teilnehmende/(?P<eventid>\w+)$',
            views.exportTeilnehmende,
            name='teilnehmende'
            ),
        url(
            r'^export/(?P<eventid>\w+)$',
            views.exportVeranstaltungen,
            name='export'
            ),
        url(
            r'^collect$',
            views.collect_veranstaltungen,
            name='collect'
            ),
        url(
            r'^accept$',
            views.accept_veranstaltungen,
            name='accept'
            ),
        url(
            r'^$',
            views.get_kommende_veranstaltungen,
            name='list'
            ),
        ]
