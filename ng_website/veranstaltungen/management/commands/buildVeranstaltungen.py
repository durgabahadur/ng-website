from django.core.management.base import BaseCommand
import veranstaltungen.helper.buildHtml as buildHtml
import veranstaltungen.helper.gCalAPI as calAPI

class Command(BaseCommand):

    def handle(self, *test_labels, **options):
        buildHtml.generateHtmlList(calAPI.getEventList())
        buildHtml.publishTemporaryHtmlList()
        pass
