import datetime as dt
import unicodecsv as csv
from xml.etree import ElementTree as ET
from django.shortcuts import render
from django.core.cache import caches
from django.core.mail import send_mail, get_connection
from django.core.mail import EmailMultiAlternatives
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from .helper.form import Anmeldung
import django.http as http
from .helper.email_text_messages import email_message
from .helper import util
from .helper import buildHtml
from .helper import gCalAPI as calAPI
import settings

def get_kommende_veranstaltungen(request):
    if request.method == 'GET':
        todayHtml = [d for d in caches['veranstaltungen'].get('daily-html-list') if d[0] >= dt.date.today()]
        dayTupleList = [buildHtml.decorateVeranstaltungen(e) for e in todayHtml]
        monthlyHtml = [ET.tostring(x, encoding="unicode", method='html') for x in buildHtml.getMonthlyList(dayTupleList)]
        context = {
            'monthList': monthlyHtml,
            'category': 'veranstaltungen',
        }
        return render(request, 'veranstaltungen.html', context)

def exportVeranstaltungen(request, eventid=None):
    try:
        event = util.getEventFromCache(eventid)
    except IndexError:
        return http.HttpResponseBadRequest('<p>kein Zutritt.</p>')

    if request.method == 'GET':
        response = http.HttpResponse(event.get('ics'), content_type='text/calendar; charset=utf-8')
        response['Filename'] = 'event.ics'
        return response

def get_veranstaltungen(request):
    if request.method == 'GET':
        return render(request, 'veranstaltungen.html')

def collect_veranstaltungen(request):
    if request.method == 'GET' and request.user.is_staff:
        eventList = calAPI.getEventList()
        caches['veranstaltungen'].set('event_list', eventList, timeout=None)
        htmlList = buildHtml.generateHtmlList(eventList)
        caches['veranstaltungen'].set('daily-html-list-temporary', htmlList, timeout=None)
        todayHtml = [d for d in htmlList if d[0] >= dt.date.today()]
        dayTupleList = [buildHtml.decorateVeranstaltungen(e) for e in todayHtml]
        monthlyHtml = [ET.tostring(x, encoding="unicode", method='html') for x in buildHtml.getMonthlyList(dayTupleList)]
        context = {
            'monthList': monthlyHtml,
        }
        return render(request, 'veranstaltungen-test.html', context)
    else:
        return http.HttpResponseForbidden('<p>kein Zutritt.</p>')

def accept_veranstaltungen(request):
    if request.method == 'GET' and request.user.is_staff:
        buildHtml.publishTemporaryHtmlList()
        return redirect('veranstaltungen:list')
    else:
        return http.HttpResponseForbidden('<p>kein Zutritt.</p>')

def formatTeilnehmer(children, adults):
    if children > 0 and adults > 0:
        return "Kinder: {} und Erwachsene: {}".format(children, adults)
    elif children > 0:
        return "Kinder: {}".format(children)
    elif adults > 0:
        return "Erwachsene: {}".format(adults)
    return "Niemand?"

def getTeilnehmende(request):
    if (request.user.is_staff):
        eventHtmlList = buildHtml.handleTeilnehmende()
        context = {
            'eventList': eventHtmlList,
        }
        return render(request, 'teilnehmendeOverview.html', context)
    else:
        return http.HttpResponseForbidden('<p>kein Zutritt.</p>')

def exportTeilnehmende(request,eventid=None):
    if request.method == 'GET' and request.user.is_staff:
        tnList = caches['teilnehmende'].get(eventid, [])
        #TODO what if tnList == 0
        if len(tnList) > 0:
            response = http.HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="teilnehmer.csv"'
            fieldnames = ['fname', 'lname', 'email', 'phone', 'bemerkung',
                          'adultNumber', 'childrenNumber']
            writer = csv.DictWriter(response, fieldnames=fieldnames)
            writer.writeheader()
            [writer.writerow(i) for i in tnList]
            return response
    return http.HttpResponseForbidden('<p>kein Zutritt.</p>')

def get_anmeldung(request,eventid=None):
    try:
        event = util.getEventFromCache(eventid)
    except IndexError:
        return http.HttpResponseBadRequest('<p>keine Veranstaltung mit dieser ID gefunden. Bitte Veranstaltungen neu laden.</p>')
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = Anmeldung(request.POST)
        # check whether it's valid:
        if form.is_valid():
            data = {
                'fname': form.cleaned_data['sender_fname'],
                'lname': form.cleaned_data['sender_lname'],
                'childrenNumber': int(form.cleaned_data['childrenNumber']),
                'adultNumber': int(form.cleaned_data['adultNumber']),
                'email': form.cleaned_data['sender_email'],
                'phone': form.cleaned_data['sender_phone']
            }
            if len(form.cleaned_data['bemerkung']) > 3:
                data['bemerkung'] = form.cleaned_data['bemerkung']
            else:
                data['bemerkung'] = "Nichts"

            cacheList = caches['teilnehmende'].get(eventid, [])
            cacheList.append(data)
            # keep seven days after the end of event, have to remove tz, else error
            delta = event['end']['datetime'].replace(tzinfo=None) - dt.datetime.now() + dt.timedelta(days=7)
            caches['teilnehmende'].set(eventid, cacheList, delta.total_seconds())
            data['summary'] = event.get('summary')
            data['zeit'] = event.get('zeitRaw')
            data['teilnehmer'] = formatTeilnehmer(
                data['childrenNumber'], data['adultNumber']
            )

            message = email_message.format(**data)
            # sophisticated mail for customer because we want to attach ics 
            # to configure our own auth user/psw we have to pass a
            # 'connection' to EmailMessage. I copied this procedure from
            # send_mail()'s source code because send_mail() is a wrapped
            # EmailMessage.send() function.
            # site-packages/django/core/mail/__init__.py

            connection = get_connection(
                username="veranstaltungen@nuglargaerten.ch",
                password=settings.common.EMAIL_PASSWORD_VERANSTALTUNGEN,
                fail_silently=False,
            )
            # return mail.send()
            msg = EmailMessage(
                event.get('summary'),
                message,
                "veranstaltungen@nuglargaerten.ch",
                [
                    form.cleaned_data['sender_email'],
                    "veranstaltungen@nuglargaerten.ch",
                    "kontakt@nuglargaerten.ch",
                    "gartenzwerge@nuglargaerten.ch",
                    ],
                connection=connection
            )
            msg.attach('event.ics', event.get('ics'), 'text/calendar; charset=utf-8')
            # simple email, we dont want to send ics to ourselves
            try:
                msg.send()
            except Exception as e: print('Error while sending email: {}'.format(e))

            return render(
                request,
                'bestaetigung.html',
                {
                    'email': form.cleaned_data['sender_email'],
                    'name': form.cleaned_data['sender_fname'],
                    'category': 'veranstaltungen'
                }
            )
    else:
        form = Anmeldung()
    return render(
        request,
        'anmeldung.html',
        {
            'form': form,
            'category': 'veranstaltungen',
            'summary': event.get('summary'),
            'zeit': event.get('zeitRaw'),
        }
    )
