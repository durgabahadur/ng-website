from django.apps import AppConfig


class VeranstaltungenConfig(AppConfig):
    name = 'veranstaltungen'
