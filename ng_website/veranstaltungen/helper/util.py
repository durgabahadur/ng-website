# -*- coding: utf-8 -*-
from django.core.cache import caches
from django.utils.dateparse import parse_datetime
import datetime as dt
from dateutil import parser
import vobject

monate = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli',
          'August', 'September', 'Oktober', 'November', 'Dezember']
wochentageKurz = ['Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa', 'So']
wochentage = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag']

def formatTime(time):
    return '{:02d}:{:02d} Uhr'.format(time.hour, time.minute)

def formatDatum(date):
    return '{}, {}. {}'.format(
        wochentage[date.weekday()],
        date.day,
        monate[date.month-1]
    )

def getEventFromCache(eventid):
    return [e for e in caches['veranstaltungen'].get('event_list') if e['eventid'] == eventid][0]

def cleanTextSnippets(text):
    # some characters would crash elementtree
    if isinstance(text, str):
        return text
    else:
        return ''

def strToDatetime(rawEventDate):
    """
    Parse date or datetime formated string and return a python datetime object.
    Google delivers start/end dates either in datetime or date format. Thus,
    instead of a pure string we get a key/value pair where the key indicates if
    we are parsing a date or a datetime string and the value is the string
    itself.
    """
    if 'date' in rawEventDate.keys():
        rawEventDate['isWholeDay'] = True
        rawEventDate['datetime'] = dt.datetime.strptime(
            rawEventDate.get('date'), '%Y-%m-%d'
        )
        rawEventDate['date'] = dt.datetime.date(rawEventDate.get('datetime'))
        rawEventDate['time'] = None
        rawEventDate['timeDisplayDe'] = ''
        rawEventDate['dateDisplayDe'] = formatDatum(rawEventDate['date'])
    elif 'dateTime' in rawEventDate.keys():
        rawEventDate['isWholeDay'] = False
        rawEventDate['datetime'] = parser.parse(rawEventDate.get('dateTime'))
        rawEventDate['date'] = dt.datetime.date(rawEventDate.get('datetime'))
        rawEventDate['time'] = dt.datetime.time(rawEventDate.get('datetime'))
        rawEventDate['timeDisplayDe'] = formatTime(rawEventDate['time'])
        rawEventDate['dateDisplayDe'] = formatDatum(rawEventDate['date'])
    return rawEventDate

def buildZeitangabeRawText(event):
    start = event.get('start')
    end = event.get('end')

    if start.get('date') == end.get('date'):
        if start.get('isWholeDay'):
            return start.get('dateDisplayDe')

        else:
            return "{}, {} -- {}".format(
                start.get('dateDisplayDe'),
                start.get('timeDisplayDe'),
                end.get('timeDisplayDe')
            )

    else:
        if start.get('isWholeDay'):
            return '{} -- {}'.format(start.get('dateDisplayDe'),end.get('dateDisplayDe'))
        else:
            return '{}, {} -- {}, {}'.format(
                start.get('dateDisplayDe'), start.get('timeDisplayDe'),
                end.get('dateDisplayDe'), end.get('timeDisplayDe')
            )


def buildZeitangabe(event):
    start = event.get('start')
    end = event.get('end')

    if start.get('date') == end.get('date'):
        if start.get('isWholeDay'):
            return """
            <time datetime="{}">{}</time>
            """.format(
                start.get('datetime'), start.get('dateDisplayDe')
            )

        else:
            return """
            <time datetime="{}">{}</time>,
            <time datetime="{}">{}</time> --
            <time datetime="{}">{}</time>
            """.format(
                start.get('dateTime'),
                start.get('dateDisplayDe'),
                start.get('dateTime'),
                start.get('timeDisplayDe'),
                end.get('dateTime'),
                end.get('timeDisplayDe')
            )

    else:
        if start.get('isWholeDay'):
            return '{} -- {}'.format(start.get('dateDisplayDe'),end.get('dateDisplayDe'))
        else:
            return '{}, {} -- {}, {}'.format(
                start.get('dateDisplayDe'), start.get('timeDisplayDe'),
                end.get('dateDisplayDe'), end.get('timeDisplayDe')
            )
def createEventIcs(event):
    cal = vobject.iCalendar()
    vevent = cal.add('vevent')
    if event.get('start')['isWholeDay']:
        vevent.add('dtstart').value = event.get('start')['date']
        if event.get('start')['date'] != event.get('end')['date']:
            vevent.add('dtend').value = event.get('end')['date']
    else:
        vevent.add('dtstart').value = event.get('start')['datetime']
        vevent.add('dtend').value = event.get('end')['datetime']
    vevent.add('summary').value = event.get('summary')
    vevent.add('location').value = event.get('location')
    vevent.add('description').value = event.get('description')
    return cal.serialize()

def saveSingleEvent(event, calname):
    start = strToDatetime(event['start'])
    end = strToDatetime(event['end'])

    # google somehow thinks that whole day events end one day after last day
    if end['isWholeDay']:
        end['datetime'] = end['datetime'] - dt.timedelta(seconds=1)
        end['date'] = end['datetime'].date()

    # google sends a html description which confuses elementTree
    # Thats why we have to replace/adjust certain tags :(
    eventDict = dict(
        kategorie = calname,
        eventid = event['id'],
        start = start,
        end = end,
        location = cleanTextSnippets(event.get('location')),
        description = cleanTextSnippets(event.get('description'))
            .replace('<br>','<br />')
            .replace('&nbsp;','\n')
            .replace('&amp;','und')
            .replace('&', 'und')
            ,
        summary = cleanTextSnippets(event.get('summary')),
        zeit = buildZeitangabe(event),
        zeitRaw = buildZeitangabeRawText(event)
    )

    # use just given info to create an iCalendar
    eventDict['ics'] = createEventIcs(eventDict)

    return eventDict
