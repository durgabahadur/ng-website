import datetime as dt
import functools as ft
from dateutil.relativedelta import relativedelta
from xml.etree import ElementTree as ET
from xml.etree.ElementTree import ParseError
from django.core.cache import caches
from django.template.loader import render_to_string
from . import util as u
from .gCalAPI import calList as calList

def foldListFunc(x,y):
    try:
        x.append(y)
    except:
        print('ERRRRv')
        print(x)
        print(y)
        print('ERRRR^')
    finally:
        return x

def collectEventClasses(event, day):
    start = dt.datetime.date(event.get('start').get('datetime'))
    end = dt.datetime.date(event.get('end').get('datetime'))
    classes = ''
    if day == start:
        classes = 'start'
    if day == end:
        classes = '{} {}'.format(classes, 'end')
    return classes

def buildHtmlOfDaySummary(event, day):
    """ Take an single event json and return a event html snippet """
    event['classes'] = collectEventClasses(event,day)
    heventHtml = u.cleanTextSnippets(render_to_string('hevent.html', event))
    try:
        eventHtml = ET.fromstring(heventHtml)
    except ParseError as e:
        print(
            'appending description failed:\n{}\nError message is:\n{}'.format(
                heventHtml,e
                )
            )
        eventHtml = '<p>{}</p><p>{}</p>'.format(heventHtml,e)
    return eventHtml

def isEventOfTheDay(event, day):
    start = dt.datetime.date(event.get('start').get('datetime'))
    end = dt.datetime.date(event.get('end').get('datetime'))
    if start <= day <= end:
        return True
    return False

def evaluateEventOrderWeight(event,day):
    start = event.get('start').get('datetime')
    end = event.get('end').get('datetime')
    if dt.datetime.date(end) == day and dt.datetime.date(start) != day:
        if isinstance(end, dt.date):
            return dt.datetime.combine(end,dt.time())
        return end
    if isinstance(start, dt.date):
        return dt.datetime.combine(start,dt.time())
    return start

def buildTeilnehmende(event):
    teilnehmerList = caches['teilnehmende'].get(event['eventid'], [])
    classes = ""
    if len(teilnehmerList) == 0:
        event['classes'] = "niemand-kommt"
    else:
        event['classes'] = "anmeldung-vorhanden"
    event['anzahl'] = len(teilnehmerList)
    event['teilnehmende'] = teilnehmerList
    html = render_to_string(
            'teilnehmende.html', event
            ).replace('\n','').replace('\t','')
    return html

def handleTeilnehmende():
    eventList = [v for v in caches['veranstaltungen'].get('event_list')
                 if v['start']['date'] > (dt.date.today() - dt.timedelta(30))]
    eventList = [buildTeilnehmende(e) for e in eventList]
    return eventList

def handleKat(kat, eventList, day):
    eventList = [e for e in eventList if kat == e.get('kategorie')]
    eventList = sorted(
            eventList,
            key=lambda e: evaluateEventOrderWeight(e, day)
            )
    htmlList = [buildHtmlOfDaySummary(e,day) for e in eventList]
    html = ET.Element('div', attrib={'class': 'g-t--12 g--4 kategorie {}'.format(kat)})
    katHtml = ft.reduce(foldListFunc, htmlList, html)
    return katHtml

def handleDay(day, eventList):
    eventsOfTheDay = [e for e in eventList if isEventOfTheDay(e,day)]
    kategorieHtml = [handleKat(kat.get('name'),eventsOfTheDay,day) for kat in calList]
    dayHtml = ft.reduce(
        foldListFunc,
        kategorieHtml,
        ET.Element('div', attrib={'class': 'alle kategorien'})
        )
    return (day, dayHtml)

def generateHtmlList(eventList):
    dayOne=dt.date(year=2019,month=2,day=3)
    # count days starting from dayOne to one year from today
    dayCount = (dt.date.today() + dt.timedelta(366) - dayOne).days
    dayList = list((dayOne+dt.timedelta(day)) for day in range(dayCount))
    dayHtml = [handleDay(d, eventList) for d in dayList]
    return dayHtml

def publishTemporaryHtmlList():
    htmlList = caches['veranstaltungen'].get('daily-html-list-temporary')
    try:
        caches['veranstaltungen'].set('daily-html-list', htmlList, timeout=None)
    except:
        pass

def decorateVeranstaltungen(dayTuple):
    classes = 'tag reihe'
    label = ET.Element('div', attrib={'class':'tag label'})

    if len(dayTuple[1].findall("./div/*")) == 0:
        classes = 'nixlos {}'.format(classes)
    else: # if nothin happening, we do not put the day on the label
        label.text = str(dayTuple[0].day)

    html = ET.Element('div', attrib={'class': classes})

    html.append(label)
    html.append(dayTuple[1])

    return (dayTuple[0], html)

def wrapOneMonth(m, dayTupleList):
    firstDay = dt.date(
            (dayTupleList[0][0]+relativedelta(months=+m)).year,
            (dayTupleList[0][0]+relativedelta(months=+m)).month,
            1
            )
    lastDay = dt.date(
            (dayTupleList[0][0]+relativedelta(months=+(m+1))).year,
            (dayTupleList[0][0]+relativedelta(months=+(m+1))).month,
            1
            )

    dayTuplesOfMonth = [dt for dt in dayTupleList if firstDay <= dt[0] and dt[0] < lastDay]
    etreeOfThisMonth = [dt[1] for dt in dayTuplesOfMonth]
    block = ET.fromstring("""<div class='monat block'></div>""")
    block.append(ET.fromstring("""
                <div class='label'><div class='text'>{}</div></div>
                """.format(u.monate[firstDay.month-1])))
    block.append(ft.reduce(
            foldListFunc,
            etreeOfThisMonth,
            ET.fromstring("""
                <div class='body'></div>
                """.format(u.monate[firstDay.month-1]))
            ))
    return block


def getMonthlyList(dayTupleList):
    monthDelta = int((dayTupleList[-1][0] - dayTupleList[0][0]).days/30)
    print('-1 0 {}, 0 0 {}, monthDelta {}'.format(dayTupleList[-1][0],dayTupleList[0][0],monthDelta))
    return [wrapOneMonth(m, dayTupleList) for m in range(0,monthDelta)]

def getFutureVeranstaltungen():
    html = ET.Element('div',
            attrib={'class': 'veranstaltungen container kommend'})
    todayHtml = [d for d in caches['veranstaltungen'].get('daily-html-list') if d >= dt.date.today()]
    dayTupleList = [decorateVeranstaltungen(e) for e in todayHtml]
    return ET.tostring(ft.reduce(
        foldListFunc, getMonthlyList(dayTupleList), html
        ), encoding="unicode", method='html')
