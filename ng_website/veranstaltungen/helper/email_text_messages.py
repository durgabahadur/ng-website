# coding: utf8
email_message = (u"""
Hallo {fname}!

Herzlichen Dank für die Anmeldung! Hier sind nochmals die Details:

Kursname:
{summary}

Datum:
{zeit}

Teilnehmer:
{teilnehmer}

Email:
{email}

Telefonnummer:
{phone}

Bemerkung:
{bemerkung}

Bei Fragen kannst Du Dich an veranstaltungen@nuglargaerten.ch wenden.

Bis bald und liebe Grüsse!

""")
