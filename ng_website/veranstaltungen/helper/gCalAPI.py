# -*- coding: utf-8 -*-
import json, jwt, requests
import datetime as dt
from functools import reduce
from urllib.error import HTTPError
from django.core.cache import caches
from django.conf import settings
from . import util

calList = [{
    "name": u"verein",
    "id": "v906b46nhh8l8pmqaiho9jf0vs@group.calendar.google.com",
    }, {
    "name": u"gemeinschaft",
    "id": "9ojo9180hubsj27nkff0eiqmjc@group.calendar.google.com",
    }, {
    "name": u"bildung",
    "id": "l0b07pqjpnldl4pmatm5d48okg@group.calendar.google.com",
    }, {
    "name": u"landwirtschaft",
    "id": "93eo5rrqcm0i687skcc0s4q7n4@group.calendar.google.com",
    }]


def evaluateEventOrderWeight(event):
    start = event.get('start').get('datetime')
    if isinstance(start, dt.date):
        return dt.datetime.combine(start,dt.time())
    return start

def getEvent(cal):
    print('requesting {}, id: {}'.format(cal["name"], cal["id"]))

    jwtToken = jwt.encode(
        {
            "iss": settings.SERVICE_ACCOUNT_CLIENT_EMAIL,
            "scope": "https://www.googleapis.com/auth/calendar.events.readonly",
            "aud": "https://www.googleapis.com/oauth2/v4/token",
            "iat": dt.datetime.utcnow(),
            "exp": dt.datetime.utcnow() + dt.timedelta(seconds=50)
        },
        settings.SERVICE_ACCOUNT_PRIVATE_KEY,
        algorithm='RS256',
        headers={"alg":"RS256","typ":"JWT"}
    )

    data = 'grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion={}'.format(jwtToken)
    getTokenResponse = requests.post(
            "https://www.googleapis.com/oauth2/v4/token",
            headers={
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
                },
            data=data
            )
    accessToken = getTokenResponse.json()['access_token']
    try:
        response = requests.get(
                'https://www.googleapis.com/calendar/v3/calendars/{}/events'.format(cal.get('id')),
                headers={ "Authorization": "Bearer {}".format(accessToken) }
                )
        response.raise_for_status()
    except HTTPError as http_err:
        print('HTTP error occurred: {}'.format(http_err))
    except Exception as err:
        print('Other error occurred: {}'.format(err))
    finally:
        print('status code:\n{}'.format(response.status_code))
        print('accessToken:\n{}'.format(accessToken))
    try:
        rawEventList = response.json()['items']
    except KeyError as err:
        print(u'error:\n{}'.format(err))
        print(u'json:\n{}'.format(response.json()))

    rawEventList = [e for e in rawEventList if e.get('status') == 'confirmed']
    prettyEventList = [util.saveSingleEvent(e,cal.get('name')) for e in rawEventList]
    prettyEventList = sorted(
            prettyEventList,
            key=lambda e: evaluateEventOrderWeight(e)
            )
    return prettyEventList

def foldList2(x,y):
    x.extend(y)
    return x

def getEventList():
    """ Get events from Google calendar API """
    eventList = reduce(foldList2, map(getEvent, calList), [])
    return eventList
