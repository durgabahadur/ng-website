# coding: utf8
from django import forms
# from django.forms import widgets

class Anmeldung(forms.Form):
    sender_fname = forms.CharField(help_text='Vorame')
    sender_lname = forms.CharField(help_text='Nachname')
    sender_email = forms.EmailField(help_text='example@example.com')
    sender_phone = forms.CharField(help_text='Telefonnummer')

    adultNumber = forms.DecimalField(
            min_value=1, max_value=2000, max_digits=4, decimal_places=0,
            widget=forms.NumberInput(),
            initial=0,
            help_text=("Wieviele Teilnehmende kommen insgesamt?")
            )

    childrenNumber = forms.DecimalField(
            min_value=0, max_value=2000, max_digits=4, decimal_places=0,
            widget=forms.NumberInput(),
            initial=0,
            help_text=("Wieviele Kinder kommen?")
            )

    bemerkung = forms.CharField(
            widget=forms.Textarea,
            help_text="Möchtest Du uns noch etwas mitteilen?",
            required=False
            )
