import json
import jwt
import requests
from datetime import datetime, timedelta

calendarList = {
        "bildung": "l0b07pqjpnldl4pmatm5d48okg@group.calendar.google.com",
        "landwirtschaft": "93eo5rrqcm0i687skcc0s4q7n4@group.calendar.google.com",
        "gemeinschaft": "9ojo9180hubsj27nkff0eiqmjc@group.calendar.google.com"
        }

jsonJwt = json.load(open('google_jwt_credentials.json', 'r'))

privateKey = b'{}'.format(jsonJwt['private_key'])
jwtToken = b'{}'.format(jwt.encode(
    {
        "iss": jsonJwt['client_email'],
        "scope": "https://www.googleapis.com/auth/calendar.events.readonly",
        "aud": "https://www.googleapis.com/oauth2/v4/token",
        "iat": datetime.utcnow(),
        "exp": datetime.utcnow() + timedelta(seconds=50)
        }, privateKey, algorithm='RS256',
    headers={"alg":"RS256","typ":"JWT"}
    ))

data = 'grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion={}'.format(jwtToken)

getTokenResponse = requests.post("https://www.googleapis.com/oauth2/v4/token",
        headers={
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            }, data=data)

print(getTokenResponse.text)
accessToken = getTokenResponse.json()['access_token']

for key, value in calendarList.iteritems():
    print('requesting {}'.format(key))
    r = requests.get(
            'https://www.googleapis.com/calendar/v3/calendars/{}/events'.format(value),
            headers={
                "Authorization": "Bearer {}".format(accessToken)
                })
    print(r.json())
