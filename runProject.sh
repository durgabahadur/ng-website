#!/bin/bash

set -e
# source ./venv/bin/activate

./makeProject.sh --watch &

cd ng_website
python manage.py runserver

# Kill backgrounded child on ctrl-c
trap 'pkill makeProject.sh' SIGINT SIGTERM EXIT
