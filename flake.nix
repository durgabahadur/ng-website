{
  description = "A very basic flake";
  inputs = {
    nixpkgs.url = github:NixOS/nixpkgs/nixos-22.05;
    flake-utils.url = "github:numtide/flake-utils";
    mach-nix = {
      url = "github:DavHau/mach-nix";
    };
    pypi-deps-db.url = "github:DavHau/pypi-deps-db";
  };
  outputs = { self, nixpkgs, flake-utils, mach-nix, pypi-deps-db}:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # pkgs = nixpkgs.legacyPackages.${system};
        pkgs = import nixpkgs { inherit system; };
        machNix = mach-nix.lib."${system}";
        devEnvironment = machNix.mkPython {
          requirements = nixpkgs.lib.strings.concatStrings [(builtins.readFile ./ng_website/requirements/base.txt) (builtins.readFile ./ng_website/requirements/local.txt)];
        };
        # dbName = "bioGeoDB";
      in rec {
        overlays = [
          (self: super: {
            biogeodb = packages.server;
          })
        ];
        packages.server = pkgs.stdenv.mkDerivation rec {
          name = "nuglar-webseite";
          env = pkgs.buildEnv { name = name; paths = buildInputs; };
          builder = builtins.toFile "builder.sh" ''
            source $stdenv/setup; ln -s $env $out
          '';
          buildInputs = with pkgs.python37Packages; [
              devEnvironment
            ];
          version = "0.0.1";
        };
        devShell = pkgs.mkShell {
          buildInputs = packages.server.buildInputs ++ [
            pkgs.less
            pkgs.man
            pkgs.git
          ];
          shellHook = ''
            source secrets.sh
            export PS1='\360\237\225\267 \u@biogeo \$ '
            '';
            };
            defaultPackage = packages.server;
        }
      );
}
