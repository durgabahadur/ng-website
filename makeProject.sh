SURFACE_VERSION=1.01

ASSETS_PATH=./assets
SCSS_PATH=$ASSETS_PATH/scss
PRJ_STATIC_PATH=./static
IMG_PATH=$PRJ_STATIC_PATH/img
CSS_RAW=./main_tmp.css
CSS_PREFIXED=$PRJ_STATIC_PATH/css/main.css

function usage {
	printf "usage: %s [--watch]\n" $0
	exit 1
}

function renderCss {

	# render css
	if [[ ! -f $CSS_RAW ]]
	then
		mkdir -p $PRJ_STATIC_PATH/css
	fi

	python -mscss --debug-info -I $SCSS_PATH \
		--images-root=$IMG_PATH -t expanded -o $CSS_RAW \
		$SCSS_PATH/ng.scss

	# ./node_modules/.bin/postcss $CSS_RAW --use autoprefixer -o $CSS_PREFIXED 
}

function execOnModification {
	while true; do
		printf "."
		local res
		let "res =  $(date +%s) - $(stat -c %Y $SCSS_PATH)"
		if [[ res -lt 5 ]];
		then
			printf "Change detected, let's render css!\n"
			renderCss
			printf "done! Waiting for change...again..."
		fi
		sleep 1
	done
}

# max 1 parameter
if [[ $# -gt 1 ]];
then
	printf "too many arguments\n"
	usage
fi


# parameter must be --watch or nothing
case "$1" in
	--watch)
		# watch uses same in/out file name
		# renderCss "-w $SCSS_PATH" ""
		printf "Welcome, let's render css!\n"
		renderCss
		printf "done! I keep polling for change.."
		execOnModification
		;;
	"")
		# one shot needs out file name
		printf "Welcome, let's render css!\n"
		printf "Raw output file is: $CSS_RAW\n"
		renderCss
		printf "Done!\n"
		;;
	*)
		usage
		;;
esac
