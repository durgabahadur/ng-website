# Nuglar Garten web page

https://nuglargaerten.ch

## Setup

### Python 3  (not tested)

```bash
python -m venv venv
source venv/bin/activate
```

### Python 2 (used in production)

```bash
virtualenv2 venv
source venv/bin/activate
```

## Installation

```bash
pip install -r ng_website/requirements/local.txt
npm install
```

## Build project

```bash
bash makeProject.sh
```

For local usage the website uses a sqlite database. This repo ships with
a database file `db.sqlite3`. If you want to start with a fresh, empty database
delete this file and apply:

```bash
python ./ng_website/manage.py migrate
```

## Run server and automatically rebuild css

```bash
bash runProject.sh
```
